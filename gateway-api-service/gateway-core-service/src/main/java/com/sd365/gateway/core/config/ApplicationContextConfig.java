package com.sd365.gateway.core.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
/**
 * @Class ApplicationContextConfig
 * @Description 全局性的配置在这里生成 例如RestTemplate，RedisTemplate ，amMqTemplate
 * @Author Administrator
 * @Date 2023-04-25  11:01
 * @version 1.0.0
 */
@Configuration
@Slf4j
public class ApplicationContextConfig {
    /**
     * 调用认证服务使用此组件
     * @return 用于调用认证服务和鉴权服务
     */
    @Bean
    @LoadBalanced
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }
}
