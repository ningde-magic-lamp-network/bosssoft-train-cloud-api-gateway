package com.sd365.gateway.authorization.service;

import com.sd365.permission.centre.entity.Resource;

import java.util.List;

/**
 * @InterfaceName: AuthorizationService
 * @Description
 * @Author Administrator
 * @Date 2023-04-25  11:46
 * @version 1.0.0
 */
public interface AuthorizationService {

    /**
     *  鉴权判断用户访问的uri是否在token定义中
     *  实现方法：
     *  1 首先判断 token是否过期或者即将过期
     *  2 解析token 得到角色
     *  3 通过角色从 redis取得 角色对应的资源，如果redis没有 则调用用户中心获取资源列表，
     *  4 将访问的url和资源匹配，判断是否符合，如果符合则返回true通过 否则不通过返回false
     * @param token  用户的token
     * @return 参考 BusinessResultConsts 说明
     */
    Integer roleAuthorization(String token,String api) ;


    /**
     *  根据角色从缓存或者远程接口取得资源id
     *  从缓存先取得 如果没有则从数据库取得 如果没有则从用户中心接口取得
     * @param roleIds 角色数组
     * @return 角色数组所包含的资源id
     */
    List<Resource> getResourcesByRoleIds(List<Long> roleIds);

    /**
     * 匹配用户访问的url 和 用户角色所拥有的资源列表的
     * @param url 访问的url
     * @param resources 角色所拥有的资源列表
     * @return true成功 false失败
     */
    boolean matchRequestUrlWithResources(String url, List<Resource> resources);
    /**
     * 查询请求的url是否在通用的资源列表中
     * @param  url 请求的url
     * @return true 在列表 false不在
     */
    Boolean commonResource(String url);

}
