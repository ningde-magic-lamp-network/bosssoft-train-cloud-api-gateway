package com.sd365.gateway.authentication.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.sd365.common.core.common.exception.BusinessException;
import com.sd365.common.core.common.exception.code.ThirdPartyApiErrorCode;
import com.sd365.common.util.JwtTokenBuilder;
import com.sd365.gateway.authentication.service.AuthenticationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
/**
 * @Class AuthenticationServiceImpl
 * @Description abel.zhan 2022-10-12 重构认证代码
 *  认证服务以下功能：
 *  1 调用用户中心返回认证的结果
 *  2 调用工具类生成JWT token
 *  3  设置 token的续约
 * @Author Administrator
 * @Date 2022-10-12  20:08
 * @version 1.0.0
 */
@Slf4j
@RefreshScope
@Service
public class AuthenticationServiceImpl implements AuthenticationService {

    /**
     * Jwt Token签名秘钥
     */
    @Value("${app.secret.token.key}")
    private String secret="$6$bosssoft$";
    /**
     * common包中封装的用于jwttoken构建和签名
     */
    @Autowired
    private JwtTokenBuilder jwtTokenBuilder;
    /**
     * 定义了认证接口的URL
     */
    public static final   String AUTH_URL = "http://sd365-permission-center/permission/centre/v1/user/auth?code=%s&account=%s&password=%s";
    /**
     * RedisTemplateConfig 中生成
     */
    @Resource(name = "tokenRedisTemplate")
    private RedisTemplate redisTemplate;

    /**
     * CurrentServiceTemplateBeanConfig.java中定义
     */
    @Autowired
    private RestTemplate restTemplate;

    /**
     * 默认设置10天的token有效期
     */
    @Value("${app.secret.token.period}")
    private Long period = 864000L;
    /**
     * 默认设置一天的提前提醒时间 举例如果有效期10天则在第9天提醒
     */
    @Value("${app.secret.token.remind}")
    private Long remind=86400L;
    /**
     * 设置 user的token的key
     */
    private static final String TOKEN_USER_ID_KEY = "remote:cache:token:user:";

    /**
     *  公共应答体body部分就是包含 UserVO
     */
    private static final String COMMON_RESPONSE_BODY="body";

    /**
     *  公共应答体head部分
     */
    private static final String COMMON_RESPONSE_HEAD="head";

    /**
     *  公共应答体head的code部分
     */
    private static final String COMMON_RESPONSE_HEAD_CODE="code";

    /**
     *  公共应答体head的code部分
     */
    private static final String COMMON_RESPONSE_HEAD_CODE_SUCCESS="0";
    /**
     *  公共应答体head部分
     */
    private static final String COMMON_RESPONSE_CODE_VALUE="head";
    /**
     *  公共应答体message部分
     */
    private static final String COMMON_RESPONSE_MESSAGE="message";
    /**
     *  公共应答体的UserVO的 code
     */
    private static final String COMMON_RESPONSE_BODY_CODE="code";

    @Override
    public HashMap doAuth(@NotBlank String code, @NotBlank String account, @NotBlank String password) {
        // 用户中心返回的CommonResponse 包含 UserVO对象
        HashMap authResult=getUserFromPermissionCentre(code,account,password);
        // 如果用户中心已返回错误码则将错误抛出改层统一异常处理
        if(!((HashMap)authResult.get(COMMON_RESPONSE_HEAD)).get(COMMON_RESPONSE_HEAD_CODE).equals(COMMON_RESPONSE_HEAD_CODE_SUCCESS)){
            throw new BusinessException(ThirdPartyApiErrorCode.INNER_CALL_ERROR,
                    String.format("用户中心发生认证错误(%s)",((HashMap)authResult).get(COMMON_RESPONSE_MESSAGE)));
        }
        // 返回签名后的jtwtoken 采用标准的 . 号隔离
        String token= createJwtToken(code, account, authResult);
        log.debug("token="+token);
        // 将签名后的token带回
        ((HashMap)((HashMap)authResult.get("body")).get("data")).put("token",token);
        // 获取用户id 设置token有效期
        renewalToken(((HashMap<String,String>)authResult.get("body")).get("id"),token);
        return authResult;
    }

    /**
     *  调用用户中心完成人员认证
     * @param code 工号
     * @param account 账号
     * @param password 密码
     * @return  用户中心返回的CommonResponse 包含 UserVO对象
     */
    private HashMap getUserFromPermissionCentre(String code,String account,String password){
        // 获取 request
//        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
//        ServletRequestAttributes attributes = (ServletRequestAttributes) requestAttributes;
//        HttpServletRequest request = attributes.getRequest();
//        log.debug("HttpServletRequest request ====>{}", attributes.getRequest());
//        // 由于restTemplate请求会导致header丢失，所以手动添加header
//        HttpHeaders headers = new HttpHeaders();
//        headers.set(HttpHeaders.USER_AGENT,request.getHeader("USER-AGENT"));
//        HttpEntity entity = new HttpEntity<>(null, headers);
        try {
            return restTemplate.exchange(String.format(AUTH_URL, code, account, password), HttpMethod.GET, null, HashMap.class).getBody();
        }catch (Exception ex){
            throw new BusinessException(ThirdPartyApiErrorCode.INNER_CALL_ERROR,"请求用户中心发生网络异常",ex);
        }
    }

    /**
     * 生成具体的JWT token生成的算法
     * @Author: Administrator
     * @DATE: 2022-10-12  20:14
     * @param: 账号密码 以及用户的租户机构公司等信息
     * @return: 生成的JWT TOKEN
     */
    private String createJwtToken(String code, String account, HashMap authResult) {
        /**
         *  用户中心返回new UserVO  ，UserVO的code值 1 代表认证通过 0 代表租户错误 2 代码账号或者密码错误
         */
        final HashMap body = (HashMap) authResult.get(COMMON_RESPONSE_BODY);// 这个就是 CommonResponse的body UserVO
        // 即是UserVO中的code
        Integer businessCode=(Integer) (body.get(COMMON_RESPONSE_BODY_CODE));
        JSONObject header=createJwtHead(JwtTokenBuilder.ALG_SHA2,"JWT");
        JSONObject payLoad= createJwtPayLoad(body,code,account);
        // 返回的报文带了JWT规范加密后的token
        return  jwtTokenBuilder.build().buildHead(header).buildPayload(payLoad)
                .buildSignature(secret).toString();
    }


    /**
     * 创建 jwttoken负载
     * @param commonResponseBody
     * @param code 登录工号
     * @param account 登录租户账号
     * @return JWT Token PayLoad
     */
    private JSONObject createJwtPayLoad(HashMap commonResponseBody, String code , String account){

        final JSONObject payLoad = new JSONObject();
        payLoad.put("account", account);
        payLoad.put("roleIds",  (ArrayList) (commonResponseBody).get("roleIds"));
        payLoad.put("tenantId", Long.parseLong((String) commonResponseBody.getOrDefault("tenantId", "-1")));
        payLoad.put("companyId", Long.parseLong((String) commonResponseBody.getOrDefault("companyId", "-1")));
        payLoad.put("orgId", Long.parseLong((String) commonResponseBody.getOrDefault("orgId", "-1")));
        payLoad.put("userId", Long.parseLong((String) commonResponseBody.getOrDefault("id", "-1")));
        payLoad.put("userName", (String) commonResponseBody.getOrDefault("name", "-1"));
        payLoad.put("code", code);
        // 设置token 3天过期
        payLoad.put("expiresAt",new Date(System.currentTimeMillis() + period*1000));
        return payLoad;
    }

    /**
     *  创建 JwtToken Head
     * @param alg 算法
     * @param type token类型
     * @return  JwtToken Heawd
     */
    private JSONObject createJwtHead(String alg,String type){
        final JSONObject header = new JSONObject();
        header.put("alg", JwtTokenBuilder.ALG_SHA2);
        header.put("typ", "JWT");
        return header;
    }

    /**
     * 使用redis设置token有效时间
     * @param id 用户id
     * @param token 该用户的有效token
     */
    private void renewalToken(String id,String token){
        // 将当前用户的token 放到edis 设置在redis中的有效时间，这样 和 token有效时间是重复的
        redisTemplate.opsForValue().set(TOKEN_USER_ID_KEY + id, token , period, TimeUnit.SECONDS);
    }


}
